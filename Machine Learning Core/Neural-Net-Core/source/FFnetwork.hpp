#ifndef FFNET_H
#define FFNET_H

#include <Eigen/Dense>
#include <map>
#include <string>
#include <iostream>
/* Local */
#include "NeuralNetModel.hpp"

/*
	WARNING: Future - Time the first iteration. Check how long it is. If it is 
	"long enough" the class should decide to save weights to memory during the
	feed forward.
*/

class FFnetwork{
public:
	FFnetwork();
	FFnetwork(NeuralNetModel model);
	~FFnetwork();

	bool initModel();

	Eigen::VectorXd feedForward(Eigen::VectorXd X_inputs);
	Eigen::VectorXd calculateErrorVector(Eigen::VectorXd X_activations, Eigen::VectorXd Y_expected_output);

	void addLayer();
	void removeLayer();
	void addModel();
	void removeModel();
	bool addActivationFunction(double (*activation)(double input));

	bool Run(int iterations);
	bool Run(float duration);
	bool Run(int iterations, float duration);
private:
	NeuralNetModel _model;

	std::map<std::string, Eigen::VectorXd> _activations;
	std::map<std::string, Eigen::VectorXd> _bias;
	std::map<std::string, Eigen::MatrixXd> _weights;
	
	/*
	// Input vector
	Eigen::VectorXd input_vector = Eigen::VectorXd::Zero(input_layer_width);

	std::cout << "Input vector: " << std::endl << input_vector << std::endl;
	// Weights
	Eigen::MatrixXd weight_matrix = Eigen::MatrixXd::Random(output_layer_width, input_layer_width);
	std::cout << "Weight matrix: " << std::endl << weight_matrix << std::endl;
	//Bias
	Eigen::VectorXd bias_vector = Eigen::VectorXd::Constant(output_layer_width, 10.0);
	std::cout << "Bias vector: " << std::endl << bias_vector << std::endl;
	// Output vector = input vector * weights + bias
	Eigen::VectorXd output_vector = (weight_matrix * input_vector) + bias_vector;
	std::cout << "OUTPUT vector: " << std::endl << bias_vector << std::endl;
	*/

	double (*activation_func)(double input);
	double (*error_func)(double input);


};
#endif