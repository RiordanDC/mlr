#include <iostream>
#include <Eigen/Dense>
#include <math.h>

/* Local */
#include "NeuralNetModel.hpp"
#include "Layer.hpp"
#include "FFnetwork.hpp"

inline double sigmoid(double input)
{
	return (1.0/(1.0 + exp(-1.0 * input)));
}

int main()
{
	/*
	Matrix3d weights = Matrix3d::Random();
	weights = (weights + Matrix3d::Constant(1.2)) * 50;
	cout << "weights =" << endl << weights << endl;
	Vector3d weight_totals(1,2,3);

	cout << "weights * weight_totals =" << endl << weights * weight_totals << endl;
	*/
	Layer layer1 = {"Input", 10, 0.0};
	Layer layer2 = {"Hidden1", 8, 10.0};
	Layer layer3 = {"Hidden2", 6, 10.0};
	Layer layer4 = {"Hidden3", 8, 10.0};
	Layer layer5 = {"Output", 10, 0.0};

	NeuralNetModel model;
	model.name = "FFNETv1";
	model.Layers.emplace_back(layer1);
	model.Layers.emplace_back(layer2);
	model.Layers.emplace_back(layer3);
	model.Layers.emplace_back(layer4);
	model.Layers.emplace_back(layer5);

	FFnetwork net(model);

	net.addActivationFunction(*sigmoid);
	
	net.initModel();
	Eigen::VectorXd output = net.feedForward(Eigen::VectorXd::Random(10));
	std::cout << output << std::endl;
/*
	int input_layer_width = 10;
	int output_layer_width = 7;

	// Input vector
	Eigen::VectorXd input_vector = Eigen::VectorXd::Zero(input_layer_width);

	std::cout << "Input vector: " << std::endl << input_vector << std::endl;
	// Weights
	Eigen::MatrixXd weight_matrix = Eigen::MatrixXd::Random(output_layer_width, input_layer_width);
	std::cout << "Weight matrix: " << std::endl << weight_matrix << std::endl;
	//Bias
	Eigen::VectorXd bias_vector = Eigen::VectorXd::Constant(output_layer_width, 10.0);
	std::cout << "Bias vector: " << std::endl << bias_vector << std::endl;
	// Output vector = input vector * weights + bias
	Eigen::VectorXd output_vector = (weight_matrix * input_vector) + bias_vector;
	std::cout << "OUTPUT vector: " << std::endl << bias_vector << std::endl;
*/
}
/*
	Code format:
		- Constants
			VAR CONSTANT_VARIABLE;
		- Iterator
			indexvariable;
		- Variable names:
			variableNames;
		- Indentation:
			int main
			{
				for()
				{
	
				}
			}

		- Parameter spacing:
			int main(int argc, char **argv, bool additonal_vars)
		- Argument name:
			argument_name
		- Function name
			mlrFunctionExample(example_argument);

*/

/*
	Neural Network Core Class Library

	A basic feed forward neural network class.
	Any model will require an:
		- Activation Function
		- Layer specifications
		- Learning function
		- Operation struct set (Specifications of the network)
			- 

	Classes:
	- Network
		- Initalize parameters:
			- Network Model data structure
			- Output Model data structre
			- Running parameters
				- GPU Acceleration
		- Inputs:
			- Data stream object
			- Batch specs
		- Outputs:
			- A function pointer is passed to the output that will
			be applied to the output stream. 

	------------------- Network Desired Use -------------------
	int main()
	{
	

	}

	Structs:
	- Operation struct set
		- Learning rate
		- Output format
		- Input format

*/

