#include <string>
#include <vector>
#include <Eigen/Dense>

/*
	Storing weights costs the most memory. This class
	was designed to move some weights from memory to
	hard memory. The goal is to have weights just-in-
	time loading where there is little delay between 
	retrieving, processing, and saving weights.
*/

class WeightMatrix{
public:
	WeightMatrix();
	~WeightMatrix();

private:

};