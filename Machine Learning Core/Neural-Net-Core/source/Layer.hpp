#ifndef LAYER
#define LAYER
#include <string>

struct Layer{
	std::string name;
	int size;
	double bias;
};
#endif