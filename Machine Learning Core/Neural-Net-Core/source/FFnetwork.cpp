#ifndef FFNET_CPP
#define FFNET_CPP
#include "FFnetwork.hpp"


FFnetwork::FFnetwork()
{

}

FFnetwork::FFnetwork(NeuralNetModel model)
{
	this->_model = model;

	int count = 0;
	for(auto const& layer: model.Layers)
	{
		std::cout << "LAYER[" << count << "][" << layer.name << "] - \t(" << layer.size << ")" << std::endl;
		count++;
	}
	
}

FFnetwork::~FFnetwork(){}

bool FFnetwork::initModel()
{
	/* Re-seed random number generator */
	srand((unsigned int) time(0)); 

	/* If no models found, return false */
	if(this->_model.Layers.empty())
	{
		return false;
	}

	/* Initialize weights and biases */
	{
		int prev_layer_size = this->_model.Layers[0].size;
		for(unsigned int layer_index = 1; layer_index <= this->_model.Layers.size() - 1; layer_index++)
		{
			auto layer = this->_model.Layers[layer_index];
			//this->_activations[layer.name] = Eigen::VectorXd(layer.size);
			this->_weights[layer.name] = Eigen::MatrixXd::Random(layer.size, prev_layer_size);

			if(layer.bias != 0.0)
			{
				this->_bias[layer.name] = Eigen::VectorXd::Constant(layer.size, layer.bias);
			}
			prev_layer_size = layer.size;
		}
	}	
}

Eigen::VectorXd FFnetwork::feedForward(Eigen::VectorXd X_inputs)
{
	/* Calculate first activation using inputs */
	Eigen::VectorXd activation = (this->_weights[this->_model.Layers[1].name] * X_inputs);

	if(this->_model.Layers[1].bias != 0.0)
	{
		activation += this->_bias[this->_model.Layers[1].name];
	}

	std::cout << "LAYER: " << this->_model.Layers[1].name << std::endl;

	/* Loop forward activations */
	for(unsigned int layer_index = 2; layer_index <= this->_model.Layers.size() - 1; layer_index++)
	{
		std::cout << "LAYER: " << this->_model.Layers[layer_index].name << std::endl;
		auto layer = this->_model.Layers[layer_index];
		activation = (this->_weights[layer.name] * activation);
		if(layer.bias != 0.0)
		{
			activation += this->_bias[layer.name];
		}
		/* Use custom activation function */
		activation = activation.unaryExpr(this->activation_func);
	}
	return activation;
}
inline double sqr(double input){return pow(input, 2);}
Eigen::VectorXd calculateErrorVector(Eigen::VectorXd X_activations, Eigen::VectorXd Y_expected_output)
{
	
	return (X_activations - Y_expected_output).unaryExpr(&sqr);
}

void FFnetwork::addLayer(){}
void FFnetwork::removeLayer(){}
void FFnetwork::addModel(){}
void FFnetwork::removeModel(){}
bool FFnetwork::addActivationFunction(double (*activation)(double input))
{
	this->activation_func = activation;
}

bool FFnetwork::Run(int iterations){}
bool FFnetwork::Run(float duration){}
bool FFnetwork::Run(int iterations, float duration){}


#endif