#ifndef NNMODEL
#define NNMODEL
#include <map>
#include <string>
#include <vector>

/* Local */
#include "Layer.hpp"


struct NeuralNetModel{
	std::string name;
	//std::map<std::string, std::string> typeMap;
	
	std::string type;
	std::vector<Layer> Layers;

};
#endif